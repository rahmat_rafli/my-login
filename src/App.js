// import logo from './logo.svg';
// import './App.css';

import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./components/Home";
import Layout from "./components/Layout";
import Login from "./components/Login";
import LoginRoute from "./components/LoginRoute";
import { GlobalProvider } from "./context/GlobalContext";

function App() {
  return (
    <>
      <BrowserRouter>
        <GlobalProvider>
          <Routes>
            <Route path="/" element={
              <Layout>
                <Home name="Rahmat" />
              </Layout>} />
            <Route path="/login" element={
              <LoginRoute>
                <Layout>
                  <Login />
                </Layout>
              </LoginRoute>
            } />
          </Routes>
        </GlobalProvider>
      </BrowserRouter>
    </>
  );
}

export default App;
