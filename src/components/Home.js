import Cookies from 'js-cookie';
import React from 'react';

const Home = (props) => {

    return (
        <>
            {Cookies.get('token') &&
                <p>Selamat Datang, {props.name}</p>
            }
            {!Cookies.get('token') &&
                <p>Selamat Datang</p>
            }
        </>
    )
}

export default Home;
