import Cookies from 'js-cookie';
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import GlobalContext from '../context/GlobalContext';

const Navbar = () => {

    const { handleLogout } = useContext(GlobalContext);

    return (
        <>
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                {!Cookies.get('token') &&
                    <li>
                        <Link to={"/login"}>Login</Link>
                    </li>
                }

                {Cookies.get('token') &&
                    <li>
                        <span onClick={handleLogout}>Logout</span>
                    </li>
                }
            </ul>
        </>
    )
}

export default Navbar
