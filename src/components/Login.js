import React, { useContext } from 'react';
import GlobalContext from '../context/GlobalContext';

const Login = () => {

    const { input, handleChange, handleLogin } = useContext(GlobalContext);

    return (
        <>
            <form onSubmit={handleLogin}>
                <br />
                <label>Email</label><br />
                <input type={'text'} name="email" onChange={handleChange} value={input.email} placeholder="Your Email" /><br /><br />
                <label>Password</label><br />
                <input type={'password'} name="password" onChange={handleChange} value={input.password} placeholder="Your Password" /><br /><br />

                <input type={'submit'} />
            </form>
        </>
    )
}

export default Login;
