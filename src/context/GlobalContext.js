import React, { createContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import Cookies from 'js-cookie';

const GlobalContext = createContext();

export const GlobalProvider = (props) => {

    let navigate = useNavigate();

    const [input, setInput] = useState({
        email: "",
        password: ""
    });

    const handleChange = (event) => {
        let value = event.target.value;
        let name = event.target.name;

        setInput({ ...input, [name]: value });
    };

    const handleLogin = (event) => {
        event.preventDefault()

        let { email, password } = input

        axios.post(`https://backendexample.sanbersy.com/api/user-login`, { email, password }).then((res) => {
            let data = res.data
            Cookies.set('token', data.token, { expires: 1 })
            navigate('/')
        }).catch((error) => {
            alert(error.message);
        });
    };

    const handleLogout = () => {
        Cookies.remove('token');
        navigate('/login');

        setInput({
            email: "",
            password: ""
        })
    };

    return (
        <>
            <GlobalContext.Provider value={
                {
                    input,
                    setInput,
                    handleChange,
                    handleLogin,
                    handleLogout
                }
            }>
                {props.children}
            </GlobalContext.Provider>
        </>
    )
}

export default GlobalContext;
